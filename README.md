LIBSVM-OCaml - LIBSVM Bindings for OCaml
========================================

---------------------------------------------------------------------------

**NOTE**: Philippe Veber is the new maintainer of this project on [GitHub](https://github.com/pveber/libsvm-ocaml).
